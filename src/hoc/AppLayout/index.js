import Footer from "../../common/Footer";
import Navbar from "../../common/Navbar";
import "../../styles/pages/AppLayout.scss";

const AppLayout = (props) => {
  return (
    <div className="main-container">
      <div className="header">
        <Navbar></Navbar>
      </div>
      <div className="menu"></div>
      <div className="main">
        <main>{props.children}</main>
      </div>
      <div className="right"></div>
      <div className="footer">
        <Footer></Footer>
      </div>
    </div>
  );
};

export default AppLayout;
