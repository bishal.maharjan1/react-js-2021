import "./styles/components/App.scss";
import routes from "./routes";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import NotFound from "./common/NotFound";

const App = (props) => {
  return (
    <Router>
      <Switch>
        {routes.map((route, index) => (
          <Route
            path={`${route.path}`}
            name={route.name}
            component={route.component}
            exact={route.exact}
            key={index}
          />
        ))}
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
};

export default App;
