import AppLayout from "../hoc/AppLayout";
import Auth from "../entities/Auth";

const routes = [
  {
    path: "/",
    component: AppLayout,
    name: "home",
    exact: true,
    isAuth: false,
  },
  {
    path: "/login",
    component: Auth,
    name: "login",
    exact: true,
    isAuth: false,
  },
  {
    path: "/signup",
    component: Auth,
    name: "login",
    exact: true,
    isAuth: false,
  },
];
export default routes;
