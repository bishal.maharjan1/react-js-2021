import "../../styles/components/FormControl.scss";

const FormControl = ({
  type,
  placeholder,
  name,
  value,
  onChange,
  label,
  ...props
}) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <input
        className="form-control"
        type={type}
        value={value}
        onChange={onChange}
        name={name}
        placeholder={placeholder}
        {...props}
      />
    </div>
  );
};

export default FormControl;
