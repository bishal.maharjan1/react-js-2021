import { useState, useEffect } from "react";
import "../../styles/components/Dropdown.scss";
import { useDropdown } from "./useDropdown";

const Dropdown = ({ ...props }) => {
  const [items, setItems] = useState(props.items);
  const [selectedItem, setSelectedItem] = useState(null);
  const [dropdownRef, showItems, setShowItems] = useDropdown();

  useEffect(() => {
    setItems(props.items);
  }, [props.items]);

  useEffect(() => {
    setSelectedItem(null);
  }, []);

  const dropDown = () => {
    if (!showItems) {
      setSelectedItem(null);
    }
    setShowItems(!showItems);
  };

  const selectItem = (item) => {
    setSelectedItem(item);
    props.onClickHandler(item)
    setShowItems(false);
  };

  const renderRow = (items) => {
    return items.map((item) => (
      <div
        key={item.id}
        onClick={() => selectItem(item)}
        className={selectedItem === item ? "selected" : "row-item"}
      >
        {item.value}
      </div>
    ));
  };

  return (
    <div className="select-box--box" ref={dropdownRef}>
      <div className="select-box--selected-item" onClick={dropDown}>
        {!props.hasIcon && selectedItem.value}
        {props.hasIcon && props.children}
      </div>
      <div
        style={{ display: showItems ? "block" : "none" }}
        className={"select-box--items"}
      >
        {items && renderRow(items)}
      </div>
    </div>
  );
};

export default Dropdown;
