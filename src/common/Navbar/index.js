import "../../styles/layout/_header.scss";
import { Camera, Settings, User } from "react-feather";
import Dropdown from "../Dropdown";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../entities/Auth/actions";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";

const Navbar = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const isLogin = useSelector((state) => state.auth.isLogin);

  const onClickHandler = (item) => {
    if (item.id === 2) {
      dispatch(logout());
    }
  };

  useEffect(() => {
    if (!isLogin) {
      console.log(isLogin)
      history.push("/login");
    }
  }, [isLogin]);

  return (
    <nav>
      <div className="logo">
        <Camera size={32} />
        <span className="title">Picks</span>
      </div>
      <div className="search-bar">
        <input type="search" placeholder="Search"></input>
      </div>
      <div className="options">
        <div style={{ width: "50px" }}>
          <User size={32} />
        </div>
        <div style={{ width: "50px" }}>
          <Dropdown
            items={[
              { value: "Settings", id: 1 },
              { value: "Logout", id: 2 },
            ]}
            hasIcon
            onClickHandler={onClickHandler}
          >
            <Settings size={32} />
          </Dropdown>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
