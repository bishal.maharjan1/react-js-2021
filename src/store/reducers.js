import { combineReducers } from "redux";
import authReducer from "../entities/Auth/reducer";

const rootReducer = combineReducers({
  auth: authReducer,
});

export default rootReducer;
