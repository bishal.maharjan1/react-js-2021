import { all } from "redux-saga/effects";
import { authSagaWatcher } from "../entities/Auth/saga";

export default function* rootSaga() {
  yield all([authSagaWatcher()]);
}
