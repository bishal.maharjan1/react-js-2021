import * as type from "./types"

export function login(user){
    return {
        type: type.LOGIN_REQUESTED,
        payload: user
    }
}

export function logout(){
    return {
        type: type.LOGOUT_REQUESTED,
    }
}

export function signup(user){
    return {
        type: type.SIGNUP_REQUESTED,
        payload: user
    }
}