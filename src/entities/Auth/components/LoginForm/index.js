import "../../../../styles/components/LoginForm.scss";
import FormControl from "../../../../common/FormControl";
import Button from "../../../../common/Button";
import { Formik } from "formik";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../../actions";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";

const LoginForm = () => {
  const dispatch = useDispatch();
  const isLogin = useSelector((state) => state.auth.isLogin);
  let history = useHistory();

  useEffect(() => {
    if (isLogin) {
      history.push("/");
    }
  }, [isLogin]);

  return (
    <div className="login-form-wrapper">
      <span className="title">Login</span>
      <Formik
        initialValues={{ email: "manager@a.com", password: "Admin123" }}
        validate={(values) => {
          const errors = {};
          if (!values.email) {
            errors.email = "Required";
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
          ) {
            errors.email = "Invalid email address";
          }

          if (values.password.length <= 6) {
            errors.password = "Password must be more than 6 letters.";
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting }) => {
          dispatch(login(values));
          setSubmitting(false);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          /* and other goodies */
        }) => (
          <form onSubmit={handleSubmit}>
            <FormControl
              type="email"
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
              name="email"
              label="Email"
              placeholder="Email"
              required
            />
            {errors.email && touched.email && (
              <p className="error-message">{errors.email}</p>
            )}
            <FormControl
              type="password"
              value={values.password}
              onChange={handleChange}
              onBlur={handleBlur}
              name="password"
              label="Password"
              placeholder="Password"
              autoComplete="on"
              required
            />
            {errors.password && touched.password && (
              <p className="error-message">{errors.password}</p>
            )}
            <Button
              type="submit"
              btnType="block"
              btnColor="black"
              style={{ marginTop: "5%" }}
              disabled={isSubmitting}
            >
              Login
            </Button>
            <div className="link-wrapper">
              <Link to="/signup" className="link">
                <span>Don't have an account?</span> Signup
              </Link>
            </div>
          </form>
        )}
      </Formik>
    </div>
  );
};

export default LoginForm;
