import "../../../../styles/components/LoginForm.scss";
import FormControl from "../../../../common/FormControl";
import Button from "../../../../common/Button";
import { Formik } from "formik";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { signup } from "../../actions";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";
const SignupForm = () => {
  const dispatch = useDispatch();
  const isLogin = useSelector((state) => state.auth.isLogin);
  let history = useHistory();

  useEffect(() => {
    if (isLogin) {
      history.push("/");
    }
  }, [isLogin]);

  return (
    <div className="login-form-wrapper">
      <span className="title">Signup</span>
      <Formik
        initialValues={{
          email: "",
          password: "",
          firstName: "",
          lastName: "",
          cpassword: "",
          username: ""
        }}
        validate={(values) => {
          const errors = {};
          if (!values.email) {
            errors.email = "Required";
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
          ) {
            errors.email = "Invalid email address";
          }

          if (!values.password) {
            errors.password = "Required";
          } else if (values.password.length <= 6) {
            errors.password = "Password must be more than 6 letters.";
          }

          if (!values.cpassword) {
            errors.cpassword = "Required";
          } else if (values.cpassword.length <= 6) {
            errors.cpassword = "Password must be more than 6 letters.";
          } else if (values.cpassword !== values.password) {
            errors.cpassword = "Password not matching.";
          }

          if (!values.firstName) {
            errors.firstName = "Required";
          }

          if (!values.lastName) {
            errors.lastName = "Required";
          }

          if (!values.username) {
            errors.username = "Required";
          }

          return errors;
        }}
        onSubmit={(values, { setSubmitting }) => {
          console.log(values);
          dispatch(signup(values));
          setSubmitting(false);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          /* and other goodies */
        }) => (
          <form onSubmit={handleSubmit}>
            <div className="half-container">
              <div className="half-input">
                <FormControl
                  type="text"
                  value={values.firstName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="firstName"
                  label="First Name"
                  placeholder="First Name"
                  required
                />
                {errors.firstName && touched.firstName && (
                  <p className="error-message">{errors.firstName}</p>
                )}
              </div>
              <div className="half-input">
                <FormControl
                  type="text"
                  value={values.lastName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="lastName"
                  label="Last Name"
                  placeholder="Last Name"
                  required
                />
                {errors.lastName && touched.lastName && (
                  <p className="error-message">{errors.lastName}</p>
                )}
              </div>
            </div>
            <div className="half-container">
              <div className="half-input">
                <FormControl
                  type="email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="email"
                  label="Email"
                  placeholder="Email"
                  required
                />
                {errors.email && touched.email && (
                  <p className="error-message">{errors.email}</p>
                )}
              </div>
              <div className="half-input">
                <FormControl
                  type="username"
                  value={values.usernae}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="username"
                  label="Username"
                  placeholder="Username"
                  required
                />
                {errors.username && touched.username && (
                  <p className="error-message">{errors.username}</p>
                )}
              </div>
            </div>
            <div className="half-container">
              <div className="half-input">
                <FormControl
                  type="password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="password"
                  label="Password"
                  placeholder="Password"
                  required
                />
                {errors.password && touched.password && (
                  <p className="error-message">{errors.password}</p>
                )}
              </div>
              <div className="half-input">
                <FormControl
                  type="password"
                  value={values.cpassword}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="cpassword"
                  label="Confirm Password"
                  placeholder="Confirm Password"
                  required
                />
                {errors.cpassword && touched.cpassword && (
                  <p className="error-message">{errors.cpassword}</p>
                )}
              </div>
            </div>
            <Button
              type="submit"
              btnType="block"
              btnColor="black"
              style={{ marginTop: "5%" }}
              disabled={isSubmitting}
            >
              Signup
            </Button>
            <div className="link-wrapper">
              <Link to="/login" className="link">
                <span>Already have an account?</span> Login
              </Link>
            </div>
          </form>
        )}
      </Formik>
    </div>
  );
};

export default SignupForm;
