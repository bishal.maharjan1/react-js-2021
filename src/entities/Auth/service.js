import defaultAxios from "axios";

const axios = defaultAxios.create({
  baseURL: "http://localhost:1337/auth/local",
  headers: { "Content-Type": "application/json" },
});

export async function loginService({ email, password }) {
  return axios
    .post("", {
      identifier: email,
      password: password,
    })
    .then((response) => {
      return response;
    })
    .catch((error) => {
      throw error;
    });
}

export async function signupService({
  email,
  password,
  firstName,
  lastName,
  username,
}) {
  return axios
    .post("/register", {
      email: email,
      password: password,
      firstName: firstName,
      lastName: lastName,
      username: username,
    })
    .then((response) => {
      return response;
    })
    .catch((error) => {
      throw error;
    });
}
