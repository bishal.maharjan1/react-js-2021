import * as type from "./types";

const initialState = {
  currentUser: null,
  token: null,
  loading: false,
  error: null,
  isLogin: false,
};

export default function authReducer(state = initialState, action) {
  console.log(action);

  switch (action.type) {
    case type.LOGIN_REQUESTED:
      return {
        ...state,
        loading: true,
      };
    case (type.LOGIN_SUCCESS, type.SIGNUP_SUCCESS):
      return {
        ...state,
        loading: false,
        currentUser: action.currentUser,
        token: action.token,
        isLogin: true,
      };
    case (type.LOGIN_FAILED, type.SIGNUP_FAILED):
      return {
        ...state,
        loading: false,
        error: action.message,
      };
    case type.LOGOUT_REQUESTED:
      return {
        ...state,
        loading: true,
      };
    case type.LOGOUT_SUCCESS:
      return {
        ...state,
        tcurrentUser: null,
        token: null,
        loading: false,
        error: null,
        isLogin: false,
      };
    case type.LOGOUT_FAILED:
      return {
        ...state,
      };
    default:
      return state;
  }
}
