import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";

import "../../styles/pages/Login.scss";
import LoginForm from "./components/LoginForm";
import SignupForm from "./components/SignupForm";
import googleLogo from "../../assets/images/search.svg";
import logo from "../../assets/images/logo.svg";

const Auth = (props) => {
  const [isLogin, setLogin] = useState(true);
  const location = useLocation();

  useEffect(() => {
    const currentPath = location.pathname;
    setLogin(currentPath === "/login");
  }, []);

  return (
    <div className="login-container">
      <section className="image-container">
        <img
          src="http://mayajourneys.com/wp-content/uploads/2019/01/DSC_0215_edited.jpg"
          alt="cover"
        ></img>
      </section>
      <section className="form-container">
        <div className="logo-container">
          <img src={logo} className="logo" alt="logo" />
        </div>
        {isLogin && <LoginForm></LoginForm>}
        {!isLogin && <SignupForm></SignupForm>}
        <div className="linebreak-container">
          <div className="hr-line-wrapper"></div>
          <span className="or-text">or</span>
          <div className="hr-line-wrapper"></div>
        </div>
        <div className="social-logins">
          <div className="button-container">
            <img
              src={googleLogo}
              alt="Google Logo"
              style={{ height: "20px" }}
            />
            <span className="social-title">Continue with Google</span>
            <span></span>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Auth;
