import { call, put, takeLatest } from "redux-saga/effects";
import * as type from "./types";
import { loginService, signupService } from "./service";

function* login({ payload }) {
  try {
    const loginResponse = yield call(loginService, payload);
    if ((loginResponse.status = 200)) {
      sessionStorage.setItem("jwt", loginResponse.jwt);
      yield put({
        type: type.LOGIN_SUCCESS,
        currentUser: loginResponse.user,
        token: loginResponse.jwt,
      });
    } else {
      yield put({
        type: type.LOGIN_FAILED,
        message: loginResponse.message[0].messages[0].message,
      });
    }
  } catch (e) {
    yield put({ type: type.LOGIN_FAILED, message: e.message });
  }
}

function* logout() {
  try {
    yield put({ type: type.LOGOUT_SUCCESS, token: null });
    sessionStorage.clear();
  } catch (e) {
    yield put({ type: type.LOGOUT_FAILED });
  }
}

function* signup({ payload }) {
  try {
    console.log(payload)
    const signupResponse = yield call(signupService, payload);
    if ((signupResponse.status = 200)) {
      sessionStorage.setItem("jwt", signupResponse.jwt);
      yield put({
        type: type.SIGNUP_SUCCESS,
        currentUser: signupResponse.user,
        token: signupResponse.jwt,
      });
    } else {
      yield put({
        type: type.SIGNUP_FAILED,
        message: signupResponse.message[0].messages[0].message,
      });
    }
  } catch (e) {
    yield put({ type: type.SIGNUP_FAILED, message: e.message });
  }
}

export function* authSagaWatcher() {
  yield takeLatest(type.LOGIN_REQUESTED, login);
  yield takeLatest(type.SIGNUP_REQUESTED, signup);
  yield takeLatest(type.LOGOUT_REQUESTED, logout);
}
